resource "keycloak_realm" "MythicTable" {
  realm        = var.state_name == "main" ? "MythicTable" : var.state_name
  enabled      = true
  ssl_required = "all"

  account_theme = "MythicTable"
  admin_theme   = "MythicTable"
  login_theme   = "MythicTable"

  registration_allowed           = true
  registration_email_as_username = false

  duplicate_emails_allowed                = false
  edit_username_allowed                   = true
  login_with_email_allowed                = true
  remember_me                             = true
  reset_password_allowed                  = true
  access_token_lifespan                   = "8h0m0s"
  access_token_lifespan_for_implicit_flow = "43200s"

  attributes = {}

  smtp_server {
    envelope_from         = ""
    from                  = "noreply@mythictable.com"
    from_display_name     = "Mythic Table"
    host                  = "smtp-relay.gmail.com"
    port                  = ""
    reply_to              = "noreply@mythictable.com"
    reply_to_display_name = ""
    ssl                   = true
    starttls              = false
  }
}