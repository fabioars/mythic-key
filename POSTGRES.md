# Keycloak with Postgres

The following instructions can be used by local developers to build a
Postgres Database with docker that will persist between docker restarts.
This is ideal for developers that cannot afford to install and maintain
a version of postgres on their machines, but still want to have a level
of persistence during development.

## Postgres Setup

### The basics

```
mkdir -p $HOME/docker/volumes/postgres
docker pull postgres
docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres
DB_ADDR=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' pg-docker` docker-compose --env-file=.env.pg up
```

### Shutting down

This single command along with the `--rm` above will make sure everything is cleaned up,

```
docker kill pg-docker
```

### Starting up again

```
docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres
```

### Running `psql`

```
docker exec -it pg-docker psql -U postgres
```

## Launching KeyCloak with Postgres

### The long way
```
#Gets the IP address of the pg-docker container in the network 'bridge'
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' pg-docker
export DB_ADDR=<The IP address from the previous command>
docker-compose --env-file=.env.pg up
```

### The short way
```
DB_ADDR=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' pg-docker` docker-compose --env-file=.env.pg up
```
